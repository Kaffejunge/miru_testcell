import matplotlib.pyplot as plt
import os, logging, csv, sys
import time

# Local imports
import palmsens.instrument
import palmsens.mscript
import palmsens.serial

logger = logging.getLogger(__name__)
stream_handler = logging.StreamHandler(sys.stdout)
logger.addHandler(stream_handler)


class PotentioStat:
    def __init__(self, com_port="COM4"):
        self.logger = logger.getChild(self.__class__.__name__)
        self.logger.setLevel(logging.DEBUG)

        self.comm = palmsens.serial.Serial(port=com_port, timeout=3)
        self.comm.open()
        self.device = palmsens.instrument.Instrument(self.comm)
        device_type = self.device.get_device_type()
        self.logger.info(f'Connected to {device_type}')

    def execute_ms_script(self, ms_script_file, csv_path=None, show_plot=False):
        # Read and send the MethodSCRIPT file.
        self.logger.info(f'Sending MethodSCRIPT: {os.path.basename(ms_script_file)}')
        self.device.send_script(ms_script_file)

        # print('Waiting for results. If you set the logging level to debug you can see Random hex numbers here :)')
        # RX: b'Pda7FC2689u;ba67DF73Ap,10,212,40\n'
        result_lines = self.device.readlines_until_end()
        curves = palmsens.mscript.parse_result_lines(result_lines)

        applied_potential = palmsens.mscript.get_values_by_column(curves, 0)
        measured_current = palmsens.mscript.get_values_by_column(curves, 1)

        if csv_path:
            with open(csv_path, 'w', newline='') as csvfile:
                writer = csv.writer(csvfile)
                writer.writerow(['Potential[V]', 'Current[A]'])
                for pot, amp in zip(applied_potential, measured_current):
                    writer.writerow([pot, amp])

        if show_plot:
            plt.plot(applied_potential, measured_current)
            plt.title('Voltammogram')
            plt.xlabel('Applied Potential (V)')
            plt.ylabel('Measured Current (A)')
            plt.minorticks_on()
            plt.show()

    def execute_ms_script_lite(self, ms_script_file):
        # Read and send the MethodSCRIPT file.
        self.logger.info(f'Sending MethodSCRIPT: {os.path.basename(ms_script_file)}')
        self.device.send_script(ms_script_file)

        # print('Waiting for results. If you set the logging level to debug you can see Random hex numbers here :)')
        # RX: b'Pda7FC2689u;ba67DF73Ap,10,212,40\n'
        result_lines = self.readlines_and_broaadcast_until_end()

    def readlines_and_broaadcast_until_end(self, signal=None, time_interval=0.1):
        """Receive all lines until an empty line is received."""
        data_list = []
        while True:
            try:
                line = self.device.readline()
                data = palmsens.mscript.parse_mscript_data_package(line)
                if data is not None:

                    data = {"time": len(data)*time_interval, "v": data[0].value, "i": data[1].value}
                    data_list.append(data)
                    if signal is not None:
                        signal.emit(data)

            except palmsens.instrument.CommunicationTimeout:
                continue
            if line == '\n':
                break
        return data

    def disconnect(self):
        self.comm.close()


if __name__ == "__main__":
    ms_script_file = r'.\scripts\example_cv.mscr'
    x = PotentioStat(com_port="COM6")
    x.execute_ms_script(ms_script_file=ms_script_file, csv_path="output.csv", show_plot=True)
    # x.execute_ms_script_lite(ms_script_file=ms_script_file)
    x.disconnect()
