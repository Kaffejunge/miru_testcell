from setuptools import find_packages, setup

NAME = 'miru_testcell'
AUTHOR = 'Finn Bork & KED'
URL = 'https://gitlab.com/Kaffejunge/miru_testcell/'
CLASSIFIERS = [
    "Programming Language :: Python :: 3",
    "Operating System :: OS Independent",
]
INSTALL_REQUIRES = [
    "contourpy",
    "cycler",
    "fonttools",
    "importlib-resources",
    "kiwisolver",
    "matplotlib",
    "packaging",
    "Pillow",
    "pyparsing",
    "pyserial",
    "python-dateutil",
    "seabreeze",
    "six",
    "zipp",
    "numpy"
]

EXCLUDE_PACKAGES = [
]

# with open('LICENSE.md') as f:
#     LICENSE = f.read()
# TODO: license file is not cross platform, need to fix encoding
LICENSE = ""

about = {"__version__": "1.0_master"}
# with open('ada_toolbox/__about__.py') as fp:
#     exec(fp.read(), about)
#
# with open("README.md", "r") as fh:
#     DESCRIPTION = fh.read()
DESCRIPTION = ""

# find packages and prefix them with the main package name
PACKAGES = find_packages(exclude=EXCLUDE_PACKAGES)

setup(
    name=NAME,
    version=about["__version__"],
    description=DESCRIPTION,
    author=AUTHOR,
    install_requires=INSTALL_REQUIRES,
    packages=PACKAGES,
    url=URL,
    license=LICENSE,
    classifiers=CLASSIFIERS,
    package_data={
        'ada_toolbox': ['analytical_lib/baselines/*.csv']
    },
    include_package_data=True

)
