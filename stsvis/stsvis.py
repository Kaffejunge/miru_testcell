from seabreeze.spectrometers import Spectrometer
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
import numpy as np
import os


class ManualController:  # for testing
    def __init__(self):
        pass

    def open_shutter(self):
        input("open shutter and press enter to continue")

    def close_shutter(self):
        input("close shutter and press enter to continue")


class STSVis:
    def __init__(self, controller, high_integration_time=800000, low_integration_time=20000):
        self.spectrometer = Spectrometer.from_first_available()
        self.controller = controller

        # micro seconds
        self._integration_time = 0
        self.high_integration_time = high_integration_time
        self.low_integration_time = low_integration_time

        self.integration_mode = "low"
        self.set_integration_time()
        self._low_dark_background = self.take_dark_background()
        self._low_light_background = self.take_light_background()

        self.integration_mode = "high"
        self.set_integration_time()
        self._dark_background = self.take_dark_background()
        self._light_background = self.take_light_background()

        self._wavelengths = None
        self._intensities = None

        # true_min, true_max = 337.79421997, 823.34546114
        self._wavelengths_range = (360, 820)

        # kicks out all values below the filter - don't use this
        self._intensity_filter = None

        # color space references
        ref_path = os.path.join(os.getcwd(), "references")
        cie_path = os.path.join(ref_path, "ciexyz64_1.csv")
        d65_path = os.path.join(ref_path, "CIE_std_illum_D65.csv")

        self.cie = np.genfromtxt(cie_path, delimiter=',')
        self.d65 = np.genfromtxt(d65_path, delimiter=',')

        self.cie = self.cie[((self.cie[:, 0] >= self.wavelengths_range[0]) & (self.cie[:, 0] <= self.wavelengths_range[1]))]
        self.d65 = self.d65[((self.d65[:, 0] >= self.wavelengths_range[0]) & (self.d65[:, 0] <= self.wavelengths_range[1]))]

        self.d65[:, 1] = self.d65[:, 1] / self.d65[:, 1].max()  # normalize spectral reference

    def set_integration_time(self):
        integration_time = self.high_integration_time if self.integration_mode == "high" else self.low_integration_time
        self.spectrometer.integration_time_micros(integration_time)
        self._integration_time = integration_time

    # shutter functions - always call these before taking a reading - don't assume shutter is in desired state
    def open_shutter(self):
        self.controller.open_shutter()

    def close_shutter(self):
        self.controller.close_shutter()

    @property
    def wavelengths(self):
        self._wavelengths = self.spectrometer.wavelengths()
        return self._wavelengths
    
    @property
    def intensities(self):
        self._intensities = self.spectrometer.intensities()
        return self._intensities
    
    @property
    def filtered_readings(self):
        wavelengths = self.wavelengths
        intensities = self.intensities

        if self.wavelengths_range:
            lower_index = np.searchsorted(wavelengths, self.wavelengths_range[0], side='right')
            higher_index = np.searchsorted(wavelengths, self.wavelengths_range[1], side='left')
            wavelengths = wavelengths[lower_index:higher_index+1]
            intensities = intensities[lower_index:higher_index+1]

        if self.intensity_filter:
            ints_mask = intensities > self.intensity_filter
            intensities = intensities[ints_mask]
            wavelengths = wavelengths[ints_mask]

        return wavelengths, intensities

    @property
    def intensity_filter(self):
        return self._intensity_filter

    @intensity_filter.setter
    def intensity_filter(self, value):
        if isinstance(value, int) or isinstance(value, float):
            self._intensity_filter = value
        else:
            raise ValueError(f"{type(value)} is an invalid type for the intensity filter")

    @property
    def spectrum(self):
        self.open_shutter()
        wavelengths, intensities = self.filtered_readings
        return np.column_stack((wavelengths, intensities))

    @property
    def light_background(self):
        return self._light_background

    def take_light_background(self):
        self.open_shutter()
        self._light_background = np.column_stack((self.filtered_readings))
        return self._light_background

    @property
    def dark_background(self):
        return self._dark_background

    def take_dark_background(self):
        self.close_shutter()
        self._dark_background = np.column_stack((self.filtered_readings))
        return self._dark_background

    @property
    def integration_time(self):
        return self._integration_time
    
    @integration_time.setter
    def integration_time(self, integration_time):
        if isinstance(integration_time, int):
            self._integration_time = integration_time
            self.spectrometer.integration_time_micros(integration_time)
        else:
            raise ValueError(f"integration time must an int in micro seconds currenlty it is a {type(integration_time)}")
        
    @property
    def wavelengths_range(self):
        return self._wavelengths_range
    
    @wavelengths_range.setter
    def wavelengths_range(self, min_max_tuple):
        if isinstance(min_max_tuple, tuple):
            self._wavelengths_range = min_max_tuple
        else:
            raise ValueError(f"wavelength range must a tuple in the form of (min_value, max_value)")
        
    def real_time_plot(self, update_time_s=0.1, duration_s=2):
        iterations = int(duration_s/update_time_s)
        for i in range(iterations):
            wavelengths, intensities = self.filtered_readings
            max_value = max(intensities) if max(intensities) else 16000
            plt.ylim([1490, max_value])
            plt.xlim([340, 825])
            plt.plot(wavelengths, intensities)
            plt.pause(update_time_s)
            plt.clf()
        plt.plot()

    def read_cie_Y_spectrum(self):
        spectrum = self.spectrum
        spectrum[:, 1] = (spectrum[:, 1] - self.dark_background[:, 1]) / (self.light_background[:, 1] - self.dark_background[:, 1])
        invalid_idx = (spectrum[:, 1] < 0) | ((spectrum[:, 1] >= 1.0) & (self.light_background[:, 1] < 1525.0)) | np.isnan(spectrum[:, 1]) | np.isinf(spectrum[:, 1])
        spectrum[invalid_idx, 1] = 0
        over_idx = (spectrum[:, 1] > 1.0)
        spectrum[over_idx, 1] = 1.0
        intp_spectrum = interp1d(spectrum[:, 0], spectrum[:, 1], kind="cubic", fill_value="extrapolate")

        final_spectrum_wavelenghts = np.array(range(self.wavelengths_range[0], self.wavelengths_range[1]+1))
        final_spectrum_intensities = intp_spectrum(final_spectrum_wavelenghts)
        final_spectrum = np.column_stack((final_spectrum_wavelenghts, final_spectrum_intensities))
        cie_d65_spectrum = np.copy(final_spectrum)
        cie_d65_spectrum[:, 1] = cie_d65_spectrum[:, 1] * self.cie[:, 2] * self.d65[:, 1]
        return cie_d65_spectrum

    def calculate_cie_Y_transmittance(self):
        cieY = self.read_cie_Y_spectrum()
        return np.sum(cieY[:, 1]) / np.sum(self.d65[:, 1] * self.cie[:, 2])


if __name__ == "__main__":
    x = STSVis(controller=ManualController())
    # x.intensity_filter = 1500
    # x.wavelengths_range = (350, 800)

    print("dark background")
    x.take_dark_background()
    print("light background")
    x.take_light_background()
    print("spectrum")
    cieY = x.read_cie_Y_spectrum()
    print(f"\n{np.sum(cieY[:, 1])}")
    print(f"\n {np.sum(x.d65[:, 1] * x.cie[:, 1])}")
    print(f"\n{np.sum(cieY[:, 1]) / np.sum(x.d65[:, 1] * x.cie[:, 2])}")
    # x.real_time_plot(duration_s=100)
    # a = np.array([199,5,10, 20])
    # print(a[0:2])



   

# import numpy as np
# import matplotlib.pyplot as plt

# plt.axis([0, 10, 0, 1])

# for i in range(10):
#     y = np.random.random()
#     plt.scatter(i, y)
#     plt.pause(0.05)

# plt.show()