# miru_testcell

This repo contains two individual Classes controlling:
 - EmStat4 potentiometer module (https://www.palmsens.com/product/emstat4m/) 
 - STSVis Spectrometer (https://www.gophotonics.com/products/spectrometers/ocean-optics-inc/44-543-sts-vis)

Created by Finn (fbork@chem.ubc.ca) in cooperation with Miru (https://mirucorp.com/about-us/).
